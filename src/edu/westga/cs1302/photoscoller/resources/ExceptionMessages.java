package edu.westga.cs1302.photoscoller.resources;

public class ExceptionMessages {
	public static final String NULL_FILENAME = "photo's file name cannot be null";
	public static final String EMPTY_FILENAME = "photo's file name cannot be empty";
	public static final String NULL_IMAGE = "photo cannot be null";
	public static final String COLLECTION_CANNOT_BE_NULL = "album cannot be null";
	public static final String FILE_NAME_NULL = "file name cannot be null";
	public static final String FILE_NAME_EMPTY = " file name cannot be empty";
	public static final String NULL_PHOTO = " photo cannot be null";
	public static final String NULL_FILE = "file cannot be null";
	public static final int FIRST_ITEM_IN_THE_LIST = 0;
	public static final int ZERO = 0;
	public static final int ONE = 1;
	public static final String CAN_NOT_BE_LESS_THAN_ZERO = "cannot be less than zero";
	public static final String NULL_ASBSOLUTEFILEPATH = "asbsolute file path cannot be null";
	public static final String EMPTY_ASBSOLUTEFILEPATH = "asbsolute file path cannot be empty";
}
