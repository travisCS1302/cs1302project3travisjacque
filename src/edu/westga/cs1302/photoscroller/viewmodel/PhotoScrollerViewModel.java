package edu.westga.cs1302.photoscroller.viewmodel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import javafx.scene.image.Image;
import edu.westga.cs1302.photoscoller.resources.ExceptionMessages;
import edu.westga.cs1302.photoscroller.model.Photo;
import edu.westga.cs1302.photoscroller.model.PhotoAlbum;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;

public class PhotoScrollerViewModel {

	private final StringProperty fileContentProperty;
	private PhotoAlbum album;
	private final StringProperty imagePathProperty;
	private final ListProperty<String> photoListProperty;
	private final ObjectProperty<String> selectedPhotoProperty;
	private Set<String> setOfNames;
	private String currentPathOfTheDisplayedImage;
	private final BooleanProperty albumEmptyProperty;
	private int indexOfTheRemovedPhoto;
	private ArrayList<String> listOfAbsoulutePaths;
	private boolean alert;

	/**
	 * Instantiates a new photo scroller viewmodel
	 * 
	 * @precondition none
	 * @postcondition all properties are instantiated
	 */
	public PhotoScrollerViewModel() {
		this.fileContentProperty = new SimpleStringProperty();
		this.imagePathProperty = new SimpleStringProperty();
		this.album = new PhotoAlbum("Photo");
		this.setOfNames = this.album.getAlbum().keySet();

		this.alert = false;
		this.photoListProperty = new SimpleListProperty<String>(FXCollections.observableArrayList(this.setOfNames));
		this.selectedPhotoProperty = new SimpleObjectProperty<String>();
		this.currentPathOfTheDisplayedImage = "";
		this.albumEmptyProperty = new SimpleBooleanProperty();
		this.albumEmptyProperty.set(true);
		this.listOfAbsoulutePaths = new ArrayList<String>();

	}

	/**
	 * returns album empty property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the album empty property
	 */
	public BooleanProperty albumEmptyProperty() {
		return this.albumEmptyProperty;
	}

	/**
	 * Gets the string property of the file
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the string property of the file
	 */
	public StringProperty fileContentProperty() {
		return this.fileContentProperty;
	}

	/**
	 * Gets the string of the alert
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the string of the alert
	 */
	public boolean getAlert() {
		return this.alert;
	}

	/**
	 * Gets the string property of the image file
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the string property of the image file
	 */
	public StringProperty imagePathProperty() {
		return this.imagePathProperty;
	}

	/**
	 * Gets the list property of the photo
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the list property of the photo
	 */
	public ListProperty<String> photoListProperty() {
		return this.photoListProperty;
	}

	/**
	 * Gets the selected photo property of the photo
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the selected photo property
	 */
	public ObjectProperty<String> selectedPhotoProperty() {
		return this.selectedPhotoProperty;
	}

	/**
	 * Gets the index of the removed item
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the index of of the removed item
	 */
	public int getIndexOfTheRemovedPhoto() {
		return this.indexOfTheRemovedPhoto;
	}

	/**
	 * Gets the file's path and it's name (file name)
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the file's path and it's name (file name)
	 */
	public String getCurrentPathOfTheDisplayedImage() {
		return this.currentPathOfTheDisplayedImage;
	}

	/**
	 * Loads the file content of the photo.
	 *
	 * @precondition file != null
	 * @postcondition none
	 * 
	 * @param file the file to be opened
	 */

	public void loadFile(File file) {
		if (file == null) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_FILE);
		}
		try (Scanner scanner = new Scanner(file)) {

			while (scanner.hasNextLine()) {
				File filePathOfThePhoto = new File(scanner.nextLine());
				Photo photo = new Photo(filePathOfThePhoto.getName(), this.convertFileToImage(filePathOfThePhoto));

				this.album.add(photo);

				this.updateTheCollection();
			}
		} catch (Exception e) {
			System.err.print(file.getName());
		}

	}

	/**
	 * Saves the file content of the photo to the specified file.
	 *
	 * @precondition asbsoluteFilePath != null AND !asbsoluteFilePath.isEmpty()
	 * @postcondition none
	 * 
	 * @param asbsoluteFilePath the name of the file to save the content to
	 */
	public void saveAsFile(String asbsoluteFilePath) {
		if (asbsoluteFilePath == null) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_ASBSOLUTEFILEPATH);
		}
		if (asbsoluteFilePath.isEmpty()) {
			throw new IllegalArgumentException(ExceptionMessages.EMPTY_ASBSOLUTEFILEPATH);
		}
		List<Photo> aList = new ArrayList<Photo>();
		aList.addAll(this.album.getAlbum().values());
		try (PrintWriter writer = new PrintWriter(asbsoluteFilePath)) {
			for (String currentFilePath : this.listOfAbsoulutePaths) {
				writer.println(currentFilePath);
			}

		} catch (IOException e) {
			System.err.println(e.getMessage());
		}

	}

	/**
	 * Adds the selected image to the list of images and displays the loaded image
	 * 
	 * @precondition
	 * @postcondition
	 * 
	 * @param file the file the user has selected
	 */
	public void addImageToListOfImages(File file) {

		Photo photo = null;
		photo = new Photo(file.getName(), this.convertFileToImage(file));
		this.album.add(photo);
		this.photoListProperty.add(photo.getFileName());
		this.listOfAbsoulutePaths.add(file.getAbsolutePath());
		this.imagePathProperty.setValue(this.currentPathOfTheDisplayedImage);
		this.updateTheCollection();
	}

	/**
	 * Removes the selected image from the list of images
	 * 
	 * @precondition
	 * @postcondition
	 * 
	 */
	public void removesImageFromTheListOfImages() {
		List<Photo> aList = new ArrayList<Photo>();
		aList.addAll(this.album.getAlbum().values());
		String fileName = this.selectedPhotoProperty.get();
		this.indexOfTheRemovedPhoto = this.photoListProperty.indexOf(fileName);

		this.album.removeByName(fileName);
		this.photoListProperty.remove(fileName);
		this.currentPathOfTheDisplayedImage = "";
		this.imagePathProperty.setValue(this.currentPathOfTheDisplayedImage);
		this.updateTheCollection();
		this.updateTheCollection();
	}

	/**
	 * Displays an informational dialog about the application (with my name on it)
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return information about the application
	 */
	public String infoAboutTheApplication() {
		Date date = new Date();
		String summary = "";

		summary += "This GUI was created by Travis Jacque and it's name is, Travis Jacque's Photo Gui. ";
		summary += "It allows users to upload their photos and photos albums. ";
		summary += "The users will be able to scroll through and remove photos.";
		summary += "The user will be able to load an album of photos and write the paths to a file. ";
		summary += "The date is " + " " + date;

		return summary;

	}

	/**
	 * Selected item's photo populates the image view
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param key of the object
	 * @return image current image of the selection
	 */
	public Image convertsTheCurrentSelectionToAnImage(String key) {
		Image image = null;
		image = this.album.getAlbum().get(key).getImageOfPhoto();
		return image;
	}

	/**
	 * Uses a file to make an image
	 * 
	 * @precondition file != null
	 * @postcondition none
	 * @param file the file of the image
	 * @return the file as an image
	 */
	public Image convertFileToImage(File file) {
		Image image = null;
		FileInputStream imageStream;
		try {
			imageStream = new FileInputStream(file.getPath());
			image = new Image(imageStream);
		} catch (FileNotFoundException e) {
			e.printStackTrace();

		}
		return image;

	}

	/**
	 * 
	 * Gets the next image in the album
	 * 
	 * @precondition selectedPhotoIndex > -1
	 * @postcondition none
	 * 
	 * @param selectedPhotoIndex the photo's index that is currently being selected
	 *                           by the user
	 * @return the next image in the list view user
	 */
	public Image getNextImage(int selectedPhotoIndex) {
		if (selectedPhotoIndex < ExceptionMessages.ZERO) {
			throw new IllegalArgumentException(ExceptionMessages.CAN_NOT_BE_LESS_THAN_ZERO);
		}
		List<Photo> aList = new ArrayList<Photo>();
		aList.addAll(this.album.getAlbum().values());

		int nextPhotoIndex = ExceptionMessages.ZERO;
		if (selectedPhotoIndex + ExceptionMessages.ONE == this.photoListProperty.size()) {
			nextPhotoIndex = ExceptionMessages.ZERO;
		} else if (selectedPhotoIndex != this.photoListProperty.size()) {
			nextPhotoIndex = selectedPhotoIndex + ExceptionMessages.ONE;
		} else if (selectedPhotoIndex == ExceptionMessages.ZERO) {
			nextPhotoIndex = ExceptionMessages.ZERO;
		}
		Photo nextPhoto = this.album.getByName(aList.get(nextPhotoIndex).getFileName());
		return nextPhoto.getImageOfPhoto();

	}

	/**
	 * 
	 * Gets the previous image in the album
	 * 
	 * @precondition selectedPhotoIndex > -1
	 * @postcondition none
	 * 
	 * @param selectedPhotoIndex the photo's index that is currently being selected
	 *                           by the user
	 * @return the previous image in the list view user
	 */
	public Image getPreviousImage(int selectedPhotoIndex) {
		if (selectedPhotoIndex < ExceptionMessages.ZERO) {
			throw new IllegalArgumentException(ExceptionMessages.CAN_NOT_BE_LESS_THAN_ZERO);
		}
		List<Photo> aList = new ArrayList<Photo>();
		aList.addAll(this.album.getAlbum().values());

		int prevPhotoIndex = ExceptionMessages.ZERO;
		if (selectedPhotoIndex + ExceptionMessages.ONE == this.photoListProperty.size()) {
			prevPhotoIndex = ExceptionMessages.ZERO;

		} else if (selectedPhotoIndex == ExceptionMessages.ZERO) {
			prevPhotoIndex = this.photoListProperty.size() - ExceptionMessages.ONE;
		} else if (selectedPhotoIndex != this.photoListProperty.size()) {
			prevPhotoIndex = selectedPhotoIndex - ExceptionMessages.ONE;
		}
		Photo nextPhoto = this.album.getByName(aList.get(prevPhotoIndex).getFileName());
		return nextPhoto.getImageOfPhoto();

	}

	/**
	 * 
	 * Gets the image that the index is connected to in the album
	 * 
	 * @precondition selectedPhotoIndex > -1
	 * @postcondition none
	 * 
	 * @param selectedPhotoIndex the photo's index that is currently being selected
	 *                           by the user
	 * @return the current image in the list view user
	 */
	public Image getImage(int selectedPhotoIndex) {
		if (selectedPhotoIndex < ExceptionMessages.ZERO) {
			throw new IllegalArgumentException(ExceptionMessages.CAN_NOT_BE_LESS_THAN_ZERO);
		}
		List<Photo> aList = new ArrayList<Photo>();
		aList.addAll(this.album.getAlbum().values());

		Photo nextPhoto = this.album.getByName(aList.get(selectedPhotoIndex).getFileName());

		return nextPhoto.getImageOfPhoto();

	}

	/**
	 * Gets the next image's index in the list
	 * 
	 * @precondition selectedPhotoIndex > -1
	 * @postcondition none
	 * @param selectedPhotoIndex the index of the next image in the list
	 * @return the next image index value
	 */
	public int getTheNextImageIndex(int selectedPhotoIndex) {
		if (selectedPhotoIndex < ExceptionMessages.ZERO) {
			throw new IllegalArgumentException(ExceptionMessages.CAN_NOT_BE_LESS_THAN_ZERO);
		}
		int nextPhotoIndex = ExceptionMessages.ZERO;
		if (selectedPhotoIndex + ExceptionMessages.ONE == this.photoListProperty.size()) {
			nextPhotoIndex = ExceptionMessages.ZERO;
		} else if (selectedPhotoIndex != this.photoListProperty.size()) {
			nextPhotoIndex = selectedPhotoIndex + ExceptionMessages.ONE;
		} else if (selectedPhotoIndex == ExceptionMessages.ZERO) {
			nextPhotoIndex = ExceptionMessages.ZERO;
		}
		return nextPhotoIndex;

	}

	/**
	 * 
	 * Gets the previous photo's index in the album
	 * 
	 * @precondition selectedPhotoIndex > -1
	 * @postcondition none
	 * 
	 * @param selectedPhotoIndex the photo's index that is currently being selected
	 *                           by the user
	 * @return the previous photo's index in the album
	 */
	public int getThePrevImageIndex(int selectedPhotoIndex) {
		if (selectedPhotoIndex < ExceptionMessages.ZERO) {
			throw new IllegalArgumentException(ExceptionMessages.CAN_NOT_BE_LESS_THAN_ZERO);
		}
		int prevPhotoIndex = ExceptionMessages.ZERO;
		if (selectedPhotoIndex == ExceptionMessages.ZERO) {
			prevPhotoIndex = this.photoListProperty.size() - ExceptionMessages.ONE;
		} else if (selectedPhotoIndex + ExceptionMessages.ONE == this.photoListProperty.size()) {
			prevPhotoIndex = selectedPhotoIndex - ExceptionMessages.ONE;
		} else if (selectedPhotoIndex != this.photoListProperty.size()) {
			prevPhotoIndex = selectedPhotoIndex - ExceptionMessages.ONE;
		}
		return prevPhotoIndex;

	}

	/**
	 * Gets the closest items after removal index
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the closest items after removal index
	 */
	public int getsTheClosestsItemsIndexAfterRemoval() {
		int stuff = ExceptionMessages.ZERO;
		if (this.getIndexOfTheRemovedPhoto() == ExceptionMessages.ZERO) {
			stuff = this.getIndexOfTheRemovedPhoto();
		} else {
			stuff = this.getIndexOfTheRemovedPhoto() - ExceptionMessages.ONE;
		}
		return stuff;
	}

	/**
	 * Removes specific photo in the list view
	 * 
	 * @precondition fileName != null AND fileName.isEmpty()
	 * @postcondition none
	 * 
	 * @param fileName the fileName of the selected photo
	 */
	public void removeSpecficPhoto(String fileName) {
		this.album.removeByName(fileName);
		this.photoListProperty.remove(fileName);
	}

	/**
	 * Gets the closest items after removal specific index
	 * 
	 * @precondition none
	 * @postcondition none
	 * @param index index of the removed object
	 * @return the closest items after removal specific index
	 */
	public int getsTheClosestsItemsIndexAfterRemovalOfTheSpecificPhoto(int index) {
		int stuff = ExceptionMessages.ZERO;
		if (index == ExceptionMessages.ZERO) {
			stuff = this.getIndexOfTheRemovedPhoto();
		} else {
			stuff = index - ExceptionMessages.ONE;
		}
		return stuff;
	}

	private void updateTheCollection() {
		this.photoListProperty().set(FXCollections.observableArrayList(this.setOfNames));
		this.albumEmptyProperty.set(this.album.size() == ExceptionMessages.ZERO);

	}

}
