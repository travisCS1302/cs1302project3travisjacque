package edu.westga.cs1302.photoscroller.view;

import java.io.File;
import java.util.Optional;

import edu.westga.cs1302.coursegui.view.AlertProperty;
import edu.westga.cs1302.photoscoller.resources.ExceptionMessages;
import edu.westga.cs1302.photoscroller.viewmodel.PhotoScrollerViewModel;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Window;

public class PhotoScrollerCodeBehind {
	private final AlertProperty alertProperty;
	@FXML
	private AnchorPane guiPane;

	@FXML
	private MenuItem fileLoadMenuItem;

	@FXML
	private MenuItem fileSaveMenuItem;

	@FXML
	private MenuItem fileAddMenuItem;

	@FXML
	private MenuItem helpAboutMenuItem;

	@FXML
	private ImageView photoImageView;

	@FXML
	private ListView<String> photoListView;

	@FXML
	private Button previousButton;

	@FXML
	private Button nextButton;

	@FXML
	private Button removeButton;

	@FXML
	private MenuItem addImageContextMenuItem;

	@FXML
	private MenuItem removeImageContextMenuItem;

	private PhotoScrollerViewModel viewModel;

	private StringProperty holdTheImagePath;

	/**
	 * Instantiates a new code behind
	 */
	public PhotoScrollerCodeBehind() {
		this.alertProperty = new AlertProperty();
		this.viewModel = new PhotoScrollerViewModel();
		this.holdTheImagePath = new SimpleStringProperty();
	}

	@FXML
	void initialize() {
		this.bindToViewModel();
		this.setUpListenersForSelectivePhotoProperty();
		this.setupButtonEnablings();
		this.setupListenerForAlerts();
	}

	private void setUpListenersForSelectivePhotoProperty() {
		this.photoListView.getSelectionModel().selectedItemProperty().addListener((observable, oldImage, newImage) -> {
			if (newImage != null) {
				this.photoImageView.setImage(this.viewModel.convertsTheCurrentSelectionToAnImage(newImage));
			}
		});
	}

	private void setupButtonEnablings() {

		this.nextButton.disableProperty().bind(this.viewModel.albumEmptyProperty());
		this.previousButton.disableProperty().bind(this.viewModel.albumEmptyProperty());
		this.removeButton.disableProperty().bind(this.viewModel.albumEmptyProperty());
		BooleanBinding removeSelectedImageBinding = Bindings
				.isEmpty(this.photoListView.getSelectionModel().getSelectedItems());
		this.removeImageContextMenuItem.disableProperty().bind(removeSelectedImageBinding);
	}

	@FXML
	void handleAddImageUsingTheContextMenuItem(ActionEvent event) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Add a file");
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("BMP", "*.bmp"),
				new ExtensionFilter("GIF", "*.gif"), new ExtensionFilter("JPEG", "*.jpg"),
				new ExtensionFilter("PNG", "*.png"), new ExtensionFilter("All types", "*.*"));
		Window window = this.guiPane.getScene().getWindow();
		File selectedImageFile = fileChooser.showOpenDialog(window);

		if (selectedImageFile != null) {
			this.viewModel.addImageToListOfImages(selectedImageFile);
			this.photoImageView.setImage(this.viewModel.convertFileToImage(selectedImageFile));
			this.photoListView.getSelectionModel()
					.select(this.photoListView.itemsProperty().get().indexOf(selectedImageFile.getName()));
		}

	}

	@FXML
	void handleRemoveSelectedImageUsingTheContextMenuItem() {
		int removedSpecifIndex = this.photoListView.getSelectionModel().getSelectedIndex();
		this.viewModel.removeSpecficPhoto(this.photoListView.getSelectionModel().getSelectedItem());
		this.photoImageView.setImage(null);
		this.photoListView.getSelectionModel().clearAndSelect(
				this.viewModel.getsTheClosestsItemsIndexAfterRemovalOfTheSpecificPhoto(removedSpecifIndex));
	}

	@FXML
	void handleFileSave(ActionEvent event) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Save as");
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Photo Album Files", "*.paf"),
				new ExtensionFilter("All types", "*.*"));
		Window window = this.guiPane.getScene().getWindow();
		File selectedFile = fileChooser.showSaveDialog(window);
		if (selectedFile != null) {
			this.viewModel.saveAsFile(selectedFile.getAbsolutePath());
		}
	}

	@FXML
	void handleFileAdd(ActionEvent event) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Add a file");
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("BMP", "*.bmp"),
				new ExtensionFilter("GIF", "*.gif"), new ExtensionFilter("JPEG", "*.jpg"),
				new ExtensionFilter("PNG", "*.png"), new ExtensionFilter("All types", "*.*"));
		Window window = this.guiPane.getScene().getWindow();
		File selectedImageFile = fileChooser.showOpenDialog(window);

		if (selectedImageFile != null) {
			this.checksFileNameForDuplicates(selectedImageFile.getName());
			this.viewModel.addImageToListOfImages(selectedImageFile);
			this.photoImageView.setImage(this.viewModel.convertFileToImage(selectedImageFile));
			this.photoListView.getSelectionModel()
					.select(this.photoListView.itemsProperty().get().indexOf(selectedImageFile.getName()));

		}

	}

	@FXML
	void handleHelpAbout(ActionEvent event) {
		String summary = this.viewModel.infoAboutTheApplication();
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Information about the application");
		alert.setContentText(summary);
		alert.showAndWait();
	}

	@FXML
	void handleLoadFile(ActionEvent event) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Load a file");
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Photo Album Files", "*.paf"),
				new ExtensionFilter("All types", "*.*"));
		Window window = this.guiPane.getScene().getWindow();
		File file = fileChooser.showOpenDialog(window);
		if (file != null) {
			this.viewModel.loadFile(file);
			this.photoListView.getSelectionModel().select(ExceptionMessages.FIRST_ITEM_IN_THE_LIST);
			this.photoListView.getFocusModel().focus(ExceptionMessages.FIRST_ITEM_IN_THE_LIST);
		}

	}

	@FXML
	void handleNextButton(ActionEvent event) {

		Image nextImage = this.viewModel.getNextImage((this.photoListView.getSelectionModel().getSelectedIndex()));
		this.photoImageView.setImage(nextImage);
		this.photoListView.getSelectionModel().clearAndSelect(
				this.viewModel.getTheNextImageIndex(this.photoListView.getSelectionModel().getSelectedIndex()));

	}

	@FXML
	void handlePreviousButton(ActionEvent event) {
		Image nextImage = this.viewModel.getPreviousImage((this.photoListView.getSelectionModel().getSelectedIndex()));
		this.photoImageView.setImage(nextImage);
		this.photoListView.getSelectionModel().clearAndSelect(
				this.viewModel.getThePrevImageIndex(this.photoListView.getSelectionModel().getSelectedIndex()));

	}

	@FXML
	void handleRemoveButton(ActionEvent event) {
		this.viewModel.removesImageFromTheListOfImages();
		this.photoImageView.setImage(null);
		this.photoListView.getSelectionModel().clearAndSelect(this.viewModel.getsTheClosestsItemsIndexAfterRemoval());
	}

	private void showAlert(Alert.AlertType alertType) {
		Alert alert = new Alert(alertType);
		Window owner = this.guiPane.getScene().getWindow();
		alert.initOwner(owner);
		if (!this.alertProperty.getTitle().isEmpty()) {
			alert.setTitle(this.alertProperty.getTitle());
		}
		alert.setHeaderText(this.alertProperty.getHeader());
		alert.setContentText(this.alertProperty.getContent());
		Optional<ButtonType> alertResult = alert.showAndWait();
		if (alertType == AlertType.CONFIRMATION && alertResult.get() == ButtonType.CANCEL) {
			this.alertProperty.setResult("cancel");
		} else {
			this.alertProperty.setResult("ok");
		}
		this.alertProperty.setType(AlertProperty.NO_ALERT);
	}

	private void setupListenerForAlerts() {
		this.alertProperty.addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if (newValue.intValue() == AlertProperty.ERROR) {
					PhotoScrollerCodeBehind.this.showAlert(AlertType.ERROR);
				}

			}
		});
	}

	private void checksFileNameForDuplicates(String fileName) {
		if (this.photoListView.getSelectionModel().getSelectedItems().contains(fileName)) {
			this.alertProperty.set(AlertProperty.ERROR, "Duplicate File error", "ERROR: File is already in the list");
		}
	}

	private void bindToViewModel() {
		this.photoListView.itemsProperty().bind(this.viewModel.photoListProperty());
		this.viewModel.selectedPhotoProperty().bind(this.photoListView.getSelectionModel().selectedItemProperty());
		this.holdTheImagePath.bindBidirectional(this.viewModel.imagePathProperty());

	}

}
