package edu.westga.cs1302.photoscroller.model;

import edu.westga.cs1302.photoscoller.resources.ExceptionMessages;

public class PhotoFileName {

	private String fileName;

	/**
	 * Instantiates a new PhotoFileName.
	 *
	 * @precondition fileName != null && fileName is not empty
	 * @postcondition get() == fileName
	 * 
	 * @param fileName the photo's fileName
	 */
	public PhotoFileName(String fileName) {
		if (fileName == null) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_FILENAME);
		}
		if (fileName.isEmpty()) {
			throw new IllegalArgumentException(ExceptionMessages.EMPTY_FILENAME);
		}
		this.fileName = fileName;
	}

	/**
	 * Gets the string representation of the photo's file name.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the file name of the photo
	 */
	public String get() {
		return this.fileName;
	}

	/**
	 * Sets the student file name.
	 *
	 * @precondition fileName != null && fileName is not empty
	 * @postcondition get() == id
	 * 
	 * @param fileName the photo's fileName
	 */
	public void set(String fileName) {
		this.fileName = fileName;
	}

	protected void checkId(String fileName) {
		if (fileName == null) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_FILENAME);
		}
		if (fileName.isEmpty()) {
			throw new IllegalArgumentException(ExceptionMessages.EMPTY_FILENAME);
		}
	}

	@Override
	public String toString() {
		return this.fileName;
	}

}
