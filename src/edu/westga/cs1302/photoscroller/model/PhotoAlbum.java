package edu.westga.cs1302.photoscroller.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import edu.westga.cs1302.photoscoller.resources.ExceptionMessages;

/**
 * The class for the photos to be held in (Photo Album)
 * 
 * @author tjacq
 *
 */
public class PhotoAlbum implements Collection<Photo> {

	private String fileName;
	private final Map<String, Photo> album;

	/**
	 * Creates a new album to hold photos
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param fileName name of the file
	 */
	public PhotoAlbum(String fileName) {
		this.album = new HashMap<String, Photo>();
		if (fileName == null) {
			throw new IllegalArgumentException("File name cannot be null");
		}
		if (fileName.isEmpty()) {
			throw new IllegalArgumentException("File name cannot be empty");
		}
		this.fileName = fileName;

	}

	/**
	 * Gets the photo album
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the photo album
	 */
	public Map<String, Photo> getAlbum() {
		return this.album;
	}

	/**
	 * Gets the file name of this photo.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the photo with specified name
	 */
	public String getFileName() {
		return this.fileName;
	}

	/**
	 * Gets the photo with specified file name.
	 *
	 * @precondition name != null && name != empty()
	 * @postcondition none
	 * 
	 * @param fileName the fileName
	 * @return the photo with specified file name; returns null if there is no photo
	 *         with the file name
	 */
	public Photo getByName(String fileName) {

		if (fileName == null) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_FILENAME);
		}
		if (fileName.isEmpty()) {
			throw new IllegalArgumentException(ExceptionMessages.EMPTY_FILENAME);
		}

		return this.album.get(fileName);
	}

	/**
	 * Checks whether a photo with the specified name exists
	 *
	 * @precondition name != null
	 * @postcondition none
	 * 
	 * @param fileName the name of the file
	 * @return true, if the album contains the photo with the specified name
	 */
	public boolean containsFileName(String fileName) {
		if (fileName == null) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_FILENAME);
		}

		return this.album.containsKey(fileName);
	}

	/**
	 * Removes the photo with the specified name
	 *
	 * @precondition name != null
	 * @postcondition this.size() == this.size()@prev - 1
	 * 
	 * @param fileName the name of the file
	 * @return true, if the photo with the specified name has been removed
	 */
	public boolean removeByName(String fileName) {
		if (fileName == null) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_FILENAME);
		}

		return this.album.remove(fileName) != null;
	}

	@Override
	public int size() {
		return this.album.size();
	}

	@Override
	public boolean isEmpty() {
		return this.album.isEmpty();
	}

	@Override
	public boolean contains(Object photo) {
		if (photo == null) {
			throw new NullPointerException(ExceptionMessages.NULL_PHOTO);
		}
		return this.album.containsKey(((Photo) photo).getFileName());
	}

	@Override
	public Iterator<Photo> iterator() {
		return this.album.values().iterator();
	}

	@Override
	public Object[] toArray() {
		return this.album.values().toArray();
	}

	@Override
	public <T> T[] toArray(T[] album) {
		return this.album.values().toArray(album);
	}

	@Override
	public boolean add(Photo photo) {
		if (photo == null) {
			throw new NullPointerException(ExceptionMessages.NULL_PHOTO);
		}

		return this.album.put(photo.getFileName(), photo) == null;
	}

	@Override
	public boolean remove(Object photo) {
		if (photo == null) {
			throw new NullPointerException(ExceptionMessages.NULL_PHOTO);
		}
		return this.album.remove(((PhotoAlbum) photo).getFileName()) != null;
	}

	@Override
	public boolean containsAll(Collection<?> album) {

		for (Object currentPhoto : album) {
			if (currentPhoto == null) {
				throw new NullPointerException(ExceptionMessages.NULL_PHOTO);
			}
		}

		boolean changed = true;

		for (Object currentPhoto : album) {
			if (!this.contains(currentPhoto)) {
				changed = false;
			}
		}

		return changed;
	}

	@Override
	public boolean addAll(Collection<? extends Photo> album) {

		for (Photo currentPhoto : album) {
			if (currentPhoto == null) {
				throw new NullPointerException(ExceptionMessages.NULL_PHOTO);
			}
		}

		boolean changed = false;

		for (Photo currentPhoto : album) {
			if (this.add(currentPhoto)) {
				changed = true;
			}

		}
		return changed;
	}

	@Override
	public boolean removeAll(Collection<?> album) {

		for (Object currentPhoto : album) {
			if (currentPhoto == null) {
				throw new NullPointerException(ExceptionMessages.NULL_PHOTO);
			}
		}

		boolean changed = false;

		for (Object currentPhoto : album) {
			if (this.remove(currentPhoto)) {
				changed = true;
			}
		}

		return changed;
	}

	@Override
	public boolean retainAll(Collection<?> album) {

		throw new UnsupportedOperationException();
	}

	@Override
	public void clear() {
		this.album.clear();

	}

}