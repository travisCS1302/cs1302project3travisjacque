package edu.westga.cs1302.photoscroller.model;

import edu.westga.cs1302.photoscoller.resources.ExceptionMessages;
import javafx.scene.image.Image;

/**
 * Photo class
 * 
 * @author travis jacque CS1302
 *
 */
public class Photo implements Comparable<Photo> {
	private final PhotoFileName fileName;
	private Image imageOfPhoto;

	/**
	 * Instantiates a photo.
	 * 
	 * @precondition fileName != null && imageOfPhoto != null
	 * @postcondition fileName() == fileName && imageOfPhoto() == imageOfPhoto
	 * @param fileName     the name of the photo
	 * @param imageOfPhoto the image of the photo
	 */
	public Photo(String fileName, Image imageOfPhoto) {
		if (fileName == null) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_FILENAME);
		}
		if (fileName.isEmpty()) {
			throw new IllegalArgumentException(ExceptionMessages.EMPTY_FILENAME);
		}

		if (imageOfPhoto == null) {
			throw new IllegalArgumentException(ExceptionMessages.NULL_IMAGE);
		}
		// this.fileName = fileName;
		this.fileName = new PhotoFileName(fileName);
		this.imageOfPhoto = imageOfPhoto;

	}

	/**
	 * Returns the photo's file name
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the photo's file name
	 */
	public String getFileName() {
		return this.fileName.get();
	}

	/**
	 * Gets the image of the photo
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the image of the photo
	 */
	public Image getImageOfPhoto() {
		return this.imageOfPhoto;
	}

	@Override
	public int compareTo(Photo photo) {
		return this.getFileName().compareTo(photo.getFileName());
	}

}
